package design

object Scenario3 {
  /**
   * Write a class to represent and create a user
   * 
   * - User has a name/surname
   * - User has a username
   * - Username can only consist of characters "[a-z][A-Z][0-9]-._"
   * - User has a level
   * - User starts from level 0 and can only increase.
   * - User has experience
   * - User gets experience each time he posts or is reposted. 
   * - The experience transfers to levels on midnight each day
   * - The experience can't ever be negative
   * - An user is either a free user or a paid user.
   * - A free user has a limit to the amount of posts he can write per day.
   * - A paid user has a counter of the remaining paid days
   */
  class User(val name: String, val username: String, private var _remainingPaidDays: Int = 0) {
    private var _level: Int = 0
    private var _experience: Int = 0
    private var _postsRequestsToday: Int = 0
    private var _grantedPostsRequestsToday: Int = 0
    private var _maxPostsPerDay: Int = defaultMaxPostsPerDay

    if (name.isEmpty) {
      throw new IllegalArgumentException("name must not be empty")
    }
    if (username.isEmpty) {
      throw new IllegalArgumentException("username must not be empty")
    }
    if (!username.matches("^[a-zA-Z0-9-._]*$")) {
      throw new IllegalArgumentException("username must consist of characters \"[a-z][A-Z][0-9]-._\"")
    }

    def level: Int = _level
    def experience: Int = _experience
    def postsRequestsToday: Int = _postsRequestsToday
    def grantedPostsRequestsToday: Int = _grantedPostsRequestsToday
    def remainingPaidDays: Int = _remainingPaidDays
    def isPaidUser: Boolean = remainingPaidDays > 0
    def maxPostsPerDay:Int = _maxPostsPerDay

    def increaseLevelBy(increase: Int): Int = {
      if (increase < 0) {
        throw new IllegalArgumentException("level increase must be positive")
      }
      _level += increase
      level
    }

    def increaseExperienceBy(increase: Int): Int = {
      if (increase < 0) {
        throw new IllegalArgumentException("experience increase must be positive")
      }
      _experience += increase
      experience
    }

    def transferExperienceToLevel(experiencePerLevel: Int): (Int, Int) = {
      if (experiencePerLevel <= 0) {
        throw new IllegalArgumentException("experiencePerLevel must be strictly positive")
      }
      increaseLevelBy(experience / experiencePerLevel)
      _experience = experience % experiencePerLevel
      (level, experience)
    }

    def increasePaidDaysBy(increase: Int): Int = {
      if (increase < 0) {
        throw new IllegalArgumentException("paid days increase must be positive")
      }
      _remainingPaidDays += increase
      remainingPaidDays
    }

    def decreasePaidDaysByOne(): Int = {
      _remainingPaidDays = if (remainingPaidDays > 0) remainingPaidDays - 1 else 0
      remainingPaidDays
    }

    def changeMaxPostsPerDay(newMaxPostsPerDay: Int): Int = {
      if (newMaxPostsPerDay < 0) {
        throw new IllegalArgumentException("max number of posts per days must be positive")
      }
      _maxPostsPerDay = newMaxPostsPerDay
      maxPostsPerDay
    }

    def allowNewPost(): Boolean = {
      val isAllowed = isPaidUser || (_grantedPostsRequestsToday < maxPostsPerDay)
      _postsRequestsToday += 1
      if (isAllowed) _grantedPostsRequestsToday += 1
      isAllowed
    }

    def resetPostsRequests(): Unit = {
      _postsRequestsToday = 0
      _grantedPostsRequestsToday = 0
    }
  }

  val defaultMaxPostsPerDay = 3

  object UserLogic {
    /*
     * This logic will be run each midnight every day. It should:
     *   1) give a level for each 1000exp, the remaining experience goes to the next day
     *   2) if a free user is under 3 posts refresh the number of posts he can publish to 3
     *   3) paid users reduce their days remaining count
     *
     * Other functions (that you don't need to write) modify the amount of posts a user can 
     * still post when they post, give experience for posts, might increase or decrease a free users limit
     * etc.
     */
    def runAtMidnight(user: User): User = {
      user.transferExperienceToLevel(1000)
      user.resetPostsRequests()
      user.decreasePaidDaysByOne()
      user
    }
  }
}
