package review

import donotmodifyme.Scenario1._

object Scenario1 {
  /*
   * Given a `blackBoxPositiveInt` 
   *  - a black box procedure returning `Int` that's outside of our control (i.e an external library call
   * or a call to a service)
   *
   * @return the amount of calls to `blackBoxPositiveInt` needed, so that the sum of all returned values from
   * `blackBoxPositiveInt` would be equal to @input `total` 
   *
   * blackBoxPositiveInt:
   *  - can side effect
   *  - is delivered by a third party, we don't know how it operates
   */
  def process(total: Int): Int = {
    if (total < 0) {
      // MOTIVATION: we assume `blackBoxPositiveInt` will always return positive values, we must not allow the user to
      // call the method with a negative `total` argument (even if it's gracefully managed later)
      throw new IllegalArgumentException(s"total argument should not be negative (got $total)")
    }
    helper(total, 0)
  }

  def helper(total: Int, n: Int): Int = {
    // MOTIVATION: the recursion must stop if total is 0 or negative, in order to avoid a possible infinite loop (if
    // total never equals to 0
    if (total <= 0) n
    else {
      // MOTIVATION: we must assert the number of calls remains below Int.MaxValue. Otherwise, it would become negative
      // on next recursion (Int.MinValue).
      if (n == Int.MaxValue) {
        throw new Exception(s"number of calls has reached Int.MaxValue (${Int.MaxValue}) before the sum could hit total ($total)")
      }

      val newPositiveInt = blackBoxPositiveInt
      // MOTIVATION: `blackBoxPositiveInt` should always return positive values. If it does not, throw an exception
      if (newPositiveInt < 0) {
        throw new Exception(s"blackBoxPositiveInt returned negative value $newPositiveInt whereas a positive value was expected")
      }

      // Note: as we forced both total and newPositiveInt to be positive Int, there is no need to check if the difference
      // exceeded Int.MaxValue or Int.MinValue
      helper(total - newPositiveInt, n + 1)
    }
  }
}
