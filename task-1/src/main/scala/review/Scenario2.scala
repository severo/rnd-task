package review

import donotmodifyme.Scenario2._
import database._

/*
 * We need to store `donotmodifyme.Scenario2.Datum` in an efficient manner. We found a very efficient database
 * implementation (donotmodifyme.Scenario2.database.*` now we need to provide a well behaved wrapper for a
 * java the libary `database`.
 */
object Scenario2 {
  // COMMENT: possibly we should have tried to add thread safety to the methods of DatabaseUser, for example to
  // avoid two put() calls could be interleaved, resulting in one of the calls being let without effect.
  class DatabaseUser(credentials: DatabaseCredentials) {
    // MODIFICATION: add three private methods to ensure the database connection is closed if any Throwable occurs
    // COMMENT: we don't limit to catching DatabaseException because we don't want to lose access to the database.
    private def _fetch(connection: DatabaseConnection, key: String): Array[Byte] = {
      try {
        connection.fetch(key)
      } catch {
        case ex: Throwable =>
          close(connection)
          throw ex
      }
    }
    private def _put(connection: DatabaseConnection, key: String, data: Array[Byte]): Unit = {
      try {
        connection.put(key, data)
      } catch {
        case ex: Throwable =>
          close(connection)
          throw ex
      }
    }
    private def _keys(connection: DatabaseConnection): Iterator[String] = {
      try {
        connection.keys
      } catch {
        case ex: Throwable =>
          close(connection)
          throw ex
      }
    }

    def obtain: DatabaseConnection = {
      // COMMENT: don't catch a possible DatabaseException exception, since there is nothing special to do here if it
      // occurs, and because the DatabaseException message will surely give a clear explanation (runtime error,
      // unknown credentials, insufficient rights, etc.)
      DatabaseConnection.open(credentials)
    }

    // MODIFICATION: add the return type (Unit), for code coherence
    def put(connection: DatabaseConnection, datum: Datum): Unit = {
      // MODIFICATION: close the connection if an exception has been thrown.
      _put(connection, datum.key, datum.serializeContent)
    }

    def getAll(connection: DatabaseConnection): Seq[Datum] = {
      // MODIFICATION: close the connection if an exception has been thrown.
      val keys = _keys(connection).toList

      val builder = Seq.newBuilder[Datum]
      keys.foreach { key =>
        // MODIFICATION: close the connection if an exception has been thrown.
        val bytes = _fetch(connection, key)
        Datum.deserialize(bytes) match {
          case Left(error) =>
            // MODIFICATION: throw an exception in case the deserialization did not succeed instead of silently ignore
            // the datum. Note that the database connection is closed, in order to avoid being unable to later open it
            close(connection)
            throw new Exception(error)
          case Right(datum) => 
            builder += datum
        }
      }

      builder.result
    }

    def close(connection: DatabaseConnection): Unit = {
      connection.close
    }
  }
}
