package design

class UserTest extends org.scalatest.FunSuite {
  test("Primary constructor") {
    intercept[IllegalArgumentException] {
      new Scenario3.User("", "")
    }
    intercept[IllegalArgumentException] {
      new Scenario3.User("Name", "")
    }
    intercept[IllegalArgumentException] {
      new Scenario3.User("", "Username")
    }
    intercept[IllegalArgumentException] {
      new Scenario3.User("Name", "++")
    }
    val name = "My Name"
    val username = "us3R_name-9.8"
    var user = new Scenario3.User(name, username)
    assert(user.name === name)
    assert(user.username === username)
    assert(user.remainingPaidDays === 0)
    user = new Scenario3.User(name, username, 5)
    assert(user.remainingPaidDays === 5)
  }
  test("Level and experience") {
    val name = "My Name"
    val username = "us3R_name-9.8"
    val user = new Scenario3.User(name, username)
    intercept[IllegalArgumentException] {
      user.increaseLevelBy(-1)
    }
    assert(user.increaseLevelBy(0) === 0)
    assert(user.increaseLevelBy(5) === 5)

    intercept[IllegalArgumentException] {
      user.increaseExperienceBy(-1)
    }
    assert(user.increaseExperienceBy(0) === 0)
    assert(user.increaseExperienceBy(7654) === 7654)

    intercept[IllegalArgumentException] {
      user.transferExperienceToLevel(-1)
    }
    intercept[IllegalArgumentException] {
      user.transferExperienceToLevel(0)
    }
    val (newLevel, newExp) = user.transferExperienceToLevel(500)
    assert(newLevel === 20)
    assert(newExp === 154)
  }
  test("Paid days") {
    val name = "My Name"
    val username = "us3R_name-9.8"
    val user = new Scenario3.User(name, username)
    assert(user.decreasePaidDaysByOne() === 0)
    assert(user.isPaidUser === false)
    assert(user.increasePaidDaysBy(5) === 5)
    assert(user.isPaidUser === true)
    assert(user.increasePaidDaysBy(3) === 8)
    assert(user.decreasePaidDaysByOne() === 7)
    intercept[IllegalArgumentException] {
      user.increasePaidDaysBy(-1)
    }
  }
  test("Posts allowed per day") {
    val name = "My Name"
    val username = "us3R_name-9.8"
    val user = new Scenario3.User(name, username)
    intercept[IllegalArgumentException] {
      user.changeMaxPostsPerDay(-1)
    }
    assert(user.allowNewPost === true)
    assert(user.allowNewPost === true)
    assert(user.allowNewPost === true)
    assert(user.allowNewPost === false)
    user.changeMaxPostsPerDay(5)
    assert(user.allowNewPost === true)
    assert(user.allowNewPost === true)
    assert(user.allowNewPost === false)
    user.increasePaidDaysBy(1)
    assert(user.allowNewPost === true)
  }
  test("runAtMidnight") {
    val name = "My Name"
    val username = "us3R_name-9.8"
    var user = new Scenario3.User(name, username)
    // Day 1
    user = Scenario3.UserLogic.runAtMidnight(user)
    assert(user.experience === 0)
    assert(user.level === 0)
    assert(user.postsRequestsToday === 0)
    assert(user.grantedPostsRequestsToday === 0)
    assert(user.remainingPaidDays === 0)
    // Day 2
    user.increaseExperienceBy(7654)
    user.increaseLevelBy(5)
    user.increasePaidDaysBy(2)
    user = Scenario3.UserLogic.runAtMidnight(user)
    assert(user.experience === 654)
    assert(user.level === 12)
    assert(user.remainingPaidDays === 1)
  }
}