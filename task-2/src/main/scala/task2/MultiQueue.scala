package task2

import scala.util.Random
import java.util.concurrent.atomic.AtomicIntegerArray

class MultiQueue[A >: Null] private (
  // nQueues is guaranteed to be at least 2
  nQueues: Int
  // other arguments here if needed
  )(implicit ordering: Ordering[A]) {

  private val queues: Array[Queue[A]] = (0 to nQueues - 1).map( _ => Queue.empty[A] ).toArray

  private var _size: Int = 0

  private val random = new Random()

  private val locks: AtomicIntegerArray = new AtomicIntegerArray(nQueues)
  private val LOCKED = 1
  private val UNLOCKED = 0

  def compare(a: A, b: A): Boolean = {
    if (b == null) {
      true
    } else if (a == null) {
      false
    } else {
      ordering.lt(a,b)
    }
  }
  def isEmpty: Boolean = {
    _size == 0
  }
  def size: Int        = {
    _size
  }

  def insert(element: A): Unit = {
    var i = random.nextInt(nQueues)
    while (!locks.compareAndSet(i, UNLOCKED, LOCKED)) {
      i = random.nextInt(nQueues)
    }
    queues(i).enqueue(element)
    // Note: we should take care of concurrency when updating _size
    _size += 1
    locks.set(i, UNLOCKED)
  }

  /*
   * Smallest elements (non-strictly) first.
   */
  def deleteMin(): A = {
    val i = random.nextInt(nQueues)
    val j = random.nextInt(nQueues)
    // Note: maybe we should first lock both queue(i) and queue(j) in order to get a correct comparison (even if locking
    // two elements could also lead to deadlocks: one locked, and the other looping waiting for its lock)
    val k = if (compare(queues(i).peekMin, queues(j).peekMin)) i else j
    if (i != j && !locks.compareAndSet(k, UNLOCKED, LOCKED)) {
      val element = queues(k).deleteMin()
      // Note: we should take care of concurrency when updating _size
      _size -= 1
      locks.set(k, UNLOCKED)
      element
    } else {
      // As stated in the README, just throw null if lock could not be set at first try
      null
    }
  }
}

object MultiQueue {
  // You can ignore the scaling factor and the actuall amount of processors just use the given nQueues.
  def empty[A >: Null](nQueues: Int)(implicit ordering: Ordering[A]): MultiQueue[A] = {
    new MultiQueue(math.max(2, nQueues))
  }
}
