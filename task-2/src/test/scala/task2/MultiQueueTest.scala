package task2

import org.scalatest.FunSuite
import java.util.TimeZone.getAvailableIDs

class MultiQueueTest extends FunSuite {
  for (nQueues <- 0 to 5) {
    test(s"isEmpty - $nQueues") {
      val q = MultiQueue.empty[String](nQueues)(Ordering[String])
      assert(q.isEmpty === true)
      assert(q.size === 0)
      q.insert("food")
      assert(q.isEmpty === false)
      assert(q.size === 1)
    }
    test(s"single item - $nQueues") {
      val q = MultiQueue.empty[String](nQueues)(Ordering[String])
      q.insert("kiwi")
      assert(q.isEmpty === false)
      assert(q.size === 1)
      // See README - deleteMin() generally will return null
      while(!q.isEmpty) {
        val work = q.deleteMin()
        if (work != null) assert(work === "kiwi")
      }
      assert(q.isEmpty === true)
      assert(q.size === 0)
    }
    test(s"priority order - $nQueues") {
      val q = MultiQueue.empty[String](nQueues)
      for (timezone <- getAvailableIDs) {
        q.insert(timezone)
      }
      // See README - deleteMin() generally will return null
      var first = ""
      var last = ""
      var work = ""
      while(!q.isEmpty) {
        work = q.deleteMin()
        if (work != null) {
          if (first.isEmpty) first = work
          last = work
        }
      }
      // The algorithm comes with no warranty, but it's probable that the first dequeued string starts with one of the
      // first letters
      assert(first.take(1).toLowerCase < "g")
      // and the lasts one starts with one of the last letters
      assert(last.take(1).toLowerCase > "r")
    }
  }

  // TODO: test if it works in a multi-threaded scenario
}
