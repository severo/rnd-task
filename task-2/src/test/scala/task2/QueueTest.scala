package task2

import org.scalatest.FunSuite

class QueueTest extends FunSuite {
  test("isEmpty") {
    val q = Queue.empty[String]
    assert(q.isEmpty === true)
    assert(q.peekMin === null)
    q.enqueue("food")
    assert(q.isEmpty === false)
  }
  test("single item") {
    val q = Queue.empty[String]
    q.enqueue("kiwi")
    assert(q.peekMin === "kiwi")
    assert(q.isEmpty === false)
    assert(q.deleteMin() === "kiwi")
    assert(q.isEmpty === true)
  }
  test("priority order") {
    val q = Queue.empty[String]
    q.enqueue("kiwi")
    q.enqueue("banana")
    assert(q.peekMin === "banana")
    q.enqueue("watermelon")
    assert(q.peekMin === "banana")
    q.enqueue("apple")
    assert(q.peekMin === "apple")
  }
  test("deleteMin") {
    val q = Queue.empty[String]
    q.enqueue("kiwi")
    q.enqueue("banana")
    q.enqueue("watermelon")
    assert(q.peekMin === "banana")
    assert(q.deleteMin === "banana")
    assert(q.peekMin === "kiwi")
    assert(q.deleteMin === "kiwi")
    assert(q.peekMin === "watermelon")
    assert(q.deleteMin === "watermelon")
    assert(q.isEmpty === true)
    assert(q.peekMin === null)
    assert(q.deleteMin === null)
  }
}
